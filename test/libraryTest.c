#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "library.c"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

void testExercise_A() {
    // Given
    int valor[1] = {1};
    
    // When
    int* resultado = oddEvenExchangedValues(valor, 1);

    // Then
    assertArrayEquals_int(valor, 1, resultado, 1);
}

void testExercise_B() {
    // Given
    int valor [] = {1,17};
    // When
    int* resultado = oddEvenExchangedValues(valor, 2);
    // Then
    int esperado [] = {17, 1};
    assertArrayEquals_int(esperado, 2, resultado, 2);
}

void testExercise_C() {
    // Given
    int valor [] = {1,17,-9};
    // When
    int* resultado = oddEvenExchangedValues(valor, 3);
    // Then
    int esperado [] = {17, 1, -9};
    assertArrayEquals_int(esperado, 3, resultado, 3);
}

void testExercise_D() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testExercise_E() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}
