/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

public class library {
    @executerpane.MethodAnnotation(signature = "oddEvenExchangedValues(int*,int):int*")
    public int[] oddEvenExchangedValues(int[] value, int valueLength){
        return oddEvenExchangedValues_(value, valueLength);
    }
    private native int[] oddEvenExchangedValues_(int[] value, int valueLength);


    static {
        System.load(new java.io.File(".jni", "library_jni.so").getAbsolutePath());
    }
}
