#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

int* oddEvenExchangedValues(int* value, int valueLength) {
    /*
    if(valueLength > 1) {
        int swap = value[0];

        value[0] = value[1];
        value[1] = swap;
    }
    */

    int indice = 0;
   
    while(indice <= valueLength - 1){
        int swap = value[indice];

        value[indice] = value[indice + 1];
        value[indice + 1] = swap;

        indice = indice + 2;
    }

    return value;
}
